module gitlab.com/danabanana/api

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-openapi/spec v0.20.6 // indirect
	github.com/go-openapi/strfmt v0.20.1
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/streadway/amqp v1.0.0
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.8.2
	gitlab.com/danabanana/utils v0.0.1
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/sys v0.0.0-20220608164250-635b8c9b7f68 // indirect
	golang.org/x/tools v0.1.11 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.10
)

//replace gitlab.com/danabanana/utils => ../utils
