package rabbitmq

import (
	"gitlab.com/danabanana/api/pkg/config"
	"log"

	"github.com/streadway/amqp"
)

type Manager struct {
	Conn *amqp.Connection
	Chan *amqp.Channel
}

func NewRabbitMQ(config *config.Config) *amqp.Connection {
	conn, err := amqp.Dial(config.RabbitMQ.BaseURL)
	if err != nil {
		log.Fatalf("%s: %s", "Failed to connect to RabbitMQ", err)
	}
	return conn
}

func NewRabbitMQManager(conn *amqp.Connection) *Manager {
	return &Manager{Conn: conn}
}

func (m *Manager) Publisher(name string, body []byte) error {
	ch, err := m.Conn.Channel()
	m.failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		name,  // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-waits
		nil,   // arguments
	)
	m.failOnError(err, "Failed to declare a queue")

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         body,
		})
	m.failOnError(err, "Failed to publish a message")
	return nil
}

func (m *Manager) failOnError(err error, msg string) {
	if err != nil {
		log.Printf("%s: %s", msg, err)
	}
}

func (m *Manager) Consumer(name string, f func(d amqp.Delivery) error) (err error) {
	ch, err := m.Conn.Channel()
	m.failOnError(err, "Failed to open a channel")
	m.Chan = ch
	q, err := m.Chan.QueueDeclare(
		name,  // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait§
		nil,   // arguments
	)
	m.failOnError(err, "Failed to declare a queue")

	err = m.Chan.Qos(
		10,    // prefetch count
		0,     // prefetch size
		false, // global
	)
	m.failOnError(err, "Failed to set QoS")

	msgs, err := m.Chan.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	m.failOnError(err, "Failed to register a consumer")

	for d := range msgs {
		err := f(d)
		m.failOnError(err, "Failed to register a consumer")
	}
	return
}
