package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// Config model
type Config struct {
	Server struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
	} `yaml:"server"`
	DB struct {
		User            string `yaml:"user"`
		Pass            string `yaml:"pass"`
		Name            string `yaml:"name"`
		Host            string `yaml:"host"`
		Port            int    `yaml:"port"`
		Mode            string `yaml:"mode"`
		AutoMigrate     bool   `yaml:"auto_migrate"`
		AutoDataMigrate bool   `yaml:"auto_data_migrate"`
		LogMode         bool   `yaml:"log_mode"`
	} `yaml:"db"`
	RabbitMQ struct {
		BaseURL string `yaml:"baseURL"`
	} `yaml:"rabbitmq"`
	Auth struct {
		SecretKey string `yaml:"secret_key"`
		TokenTTL  int    `yaml:"token_ttl"`
	} `yaml:"auth"`
	Logger struct {
		LogLevel      string `yaml:"log_level"`
		ErrorCodesURL string `yaml:"error_codes_url"`
		Locale        string `yaml:"locale"`
	} `yaml::"logger"`
}

// Get - Config initializer
func NewConfig() *Config {
	f, err := os.Open("configs/config.yml")
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	var config Config
	err = decoder.Decode(&config)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	return &config
}

func NewTestConfig() *Config {
	filePath := fmt.Sprintf(os.Getenv("GOPATH")+"/src/gitlab.com/danabanana/api/%v", "configs/testConfig.yml")
	f, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	var config Config
	err = decoder.Decode(&config)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	return &config
}
