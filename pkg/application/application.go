package application

import (
	"gitlab.com/danabanana/api/internal/infrastructure/repository"
	"gitlab.com/danabanana/api/pkg/config"
	"gitlab.com/danabanana/api/pkg/rabbitmq"
	logV3 "gitlab.com/danabanana/utils/pkg/logger.v3"
	"gorm.io/gorm"
)

// Application model
type Application struct {
	Config   *config.Config
	RabbitMQ *rabbitmq.Manager
	Logger   *logV3.Logger
	DB       *gorm.DB
}

// NewApplication - Application initializer
func NewApplication() (*Application, error) {
	config := config.NewConfig()

	// logger
	sender, err := logV3.NewSTDSender()

	// logLevel
	logLevel := logV3.StringToLogLevel(config.Logger.LogLevel)

	logger, err := logV3.NewLogger("sample", logLevel, sender, config.Logger.ErrorCodesURL)
	if err != nil {
		return nil, err
	}

	// db connect
	gormRepo, err := repository.NewGorm(config)
	if err != nil {
		return nil, err
	}

	_amqp := rabbitmq.NewRabbitMQ(config)
	rabbitmq := rabbitmq.NewRabbitMQManager(_amqp)

	// automigrate
	if config.DB.AutoMigrate == true {
		err = gormRepo.AutoMigrate()
		if err != nil {
			return nil, err
		}
	}

	// automigrateData
	if config.DB.AutoDataMigrate == true {
		err = gormRepo.AutoMigrateData()
		if err != nil {
			return nil, err
		}
	}

	return &Application{
		Config:   config,
		Logger:   logger,
		RabbitMQ: rabbitmq,
		DB:       gormRepo.DB,
	}, nil
}

// NewTestApplication - test Application initializer
func NewTestApplication() (*Application, error) {
	config := config.NewTestConfig()

	// logger
	sender, err := logV3.NewSTDSender()

	// logLevel
	logLevel := logV3.StringToLogLevel(config.Logger.LogLevel)

	logger, err := logV3.NewLogger("test-sample", logLevel, sender, config.Logger.ErrorCodesURL)
	if err != nil {
		return nil, err
	}

	// db connect
	//gormRepo, err := repository.NewGorm(config)
	//if err != nil {
	//	return nil, err
	//}

	// automigrate
	//if config.DB.AutoMigrate == true {
	//	err = gormRepo.AutoMigrate()
	//	if err != nil {
	//		return nil, err
	//	}
	//}

	// automigrateData
	//if config.DB.AutoDataMigrate == true {
	//	err = gormRepo.AutoMigrateData()
	//	if err != nil {
	//		return nil, err
	//	}
	//}

	return &Application{
		Config: config,
		Logger: logger,
	}, nil
}
