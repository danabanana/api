package helpers

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func ParsePaginationFromQuery(ctx *gin.Context) (int, int) {
	page := 1
	size := 15

	qPage := ctx.Query("page")
	if len(qPage) > 0 {
		page, _ = strconv.Atoi(qPage)
	}

	qSize := ctx.Query("size")
	if len(qSize) > 0 {
		size, _ = strconv.Atoi(qSize)
	}

	return page, size
}
