package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	authHandler "gitlab.com/danabanana/api/internal/api/handler/http/auth"
	webHandler "gitlab.com/danabanana/api/internal/api/handler/http/web"
	"gitlab.com/danabanana/api/internal/api/middleware"
	"gitlab.com/danabanana/api/internal/infrastructure/repository"
	"gitlab.com/danabanana/api/internal/usecase/auth"
	"gitlab.com/danabanana/api/internal/usecase/web/company"
	"gitlab.com/danabanana/api/internal/usecase/web/user"
	app "gitlab.com/danabanana/api/pkg/application"
)

// NewRouter - creates new instance of gin.Engine
func NewRouter(app app.Application) (*gin.Engine, error) {
	router := gin.Default()

	// general
	router.Use(middleware.CORSMiddleware)
	v1 := router.Group("/v1")

	// auth router
	routerAuthRequired := NewAuthRouter(v1, app)

	// web router
	NewWebRouter(routerAuthRequired, app)

	// swagger docs
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router, nil
}

func NewAuthRouter(r *gin.RouterGroup, app app.Application) (routerAuthRequired *gin.RouterGroup) {
	// repos
	userRepo := repository.NewUserGormRepository(app.DB)

	//services
	userService := user.NewService(userRepo, app)
	authService := auth.NewService(userRepo, userService, app)

	authMiddleware := middleware.NewAuthMiddleware(app, authService, userService)

	authHandler.NewAuthHandler(r, app, authService)

	routerAuthRequired = r.Group("", authMiddleware.AuthorizeJWT, authMiddleware.GetUserInfo)

	return
}

func NewWebRouter(r *gin.RouterGroup, app app.Application) (web *gin.RouterGroup) {

	// repos
	companyRepo := repository.NewCompanyGormRepository(app.DB)

	//services
	companyService := company.NewService(companyRepo, app)

	// group
	web = r.Group("")

	// routes
	webHandler.NewCompanyHandler(web, app, companyService)

	return
}
