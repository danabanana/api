package http

import (
	"github.com/gin-gonic/gin"
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/usecase/web/company"
	app "gitlab.com/danabanana/api/pkg/application"
	"gitlab.com/danabanana/api/pkg/helpers"
)

type CompanyHandler struct {
	App            app.Application
	companyService company.UseCase
}

func NewCompanyHandler(r *gin.RouterGroup, App app.Application, companyService company.UseCase) {

	companyHandler := CompanyHandler{
		App:            App,
		companyService: companyService,
	}

	company := r.Group("/company")
	{
		company.POST("/", companyHandler.CreateCompany)
		company.GET("/", companyHandler.GetCompanies)
		company.GET("/:id", companyHandler.GetCompanyByID)
		company.DELETE("/:id", companyHandler.DeleteCompanyByID)
	}
}

// CreateCompany godoc
// @Tags Company
// @Summary Create Company by provided parameters
// @ID create-company
// @Security ApiKeyAuth
// @Accept json
// @Param Company	body presenter.CompanyCreateRequest 1 "Company data"
// @Produce json
// @Success 200 {object} presenter.CompanyResponse
// @Failure 400 {object} presenter.BaseResponse
// @Failure 500 {string} message
// @Router /company [post]
func (h *CompanyHandler) CreateCompany(ctx *gin.Context) {
	h.App.Logger.Trace("CreateCompany", "start")

	var companyCreateRequest presenter.CompanyCreateRequest
	if err := ctx.ShouldBindJSON(&companyCreateRequest); err != nil {
		errResp := h.App.Logger.ErrorResponse(5000, err, h.App.Config.Logger.Locale)
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	response, errResp := h.companyService.Create(companyCreateRequest)
	if errResp != nil {
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	h.App.Logger.Trace("CreateCompany", "finish", response)
	ctx.JSON(response.HTTPStatus, response)
}

// GetCompanies godoc
// @Tags Company
// @Summary Get list of companies
// @ID get-companies
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Success 200 {object} presenter.CompanyListResponse
// @Failure 400 {object} presenter.BaseResponse
// @Failure 500 {string} message
// @Router /company [get]
func (h *CompanyHandler) GetCompanies(ctx *gin.Context) {
	page, size := helpers.ParsePaginationFromQuery(ctx)
	response, errResp := h.companyService.List(page, size, "")

	if errResp != nil {
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	ctx.JSON(response.HTTPStatus, response)
}

// GetCompanyByID godoc
// @Tags Company
// @Summary Get Company by ID
// @ID get-company-by-id
// @Security ApiKeyAuth
// @Accept json
// @Param id path string true "Company ID"
// @Produce json
// @Success 200 {object} presenter.CompanyResponse
// @Failure 400 {object} presenter.BaseResponse
// @Failure 500 {string} Message
// @Router /company/{id} [get]
func (h *CompanyHandler) GetCompanyByID(ctx *gin.Context) {
	companyID := strfmt.UUID4(ctx.Param("id"))
	response, errResp := h.companyService.Get(companyID)
	if errResp != nil {
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	ctx.JSON(response.HTTPStatus, response)
}

// DeleteCompanyByID godoc
// @Tags Company
// @Summary Get Company by ID
// @ID delete-company-by-id
// @Security ApiKeyAuth
// @Accept json
// @Param id path string true "Company ID"
// @Produce json
// @Success 200 {object} presenter.BaseResponse
// @Failure 400 {object} presenter.BaseResponse
// @Failure 500 {string} Message
// @Router /company/{id} [delete]
func (h *CompanyHandler) DeleteCompanyByID(ctx *gin.Context) {
	companyID := strfmt.UUID4(ctx.Param("id"))
	response, errResp := h.companyService.Delete(companyID)
	if errResp != nil {
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	ctx.JSON(response.HTTPStatus, response)
}
