package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/usecase/auth"
	app "gitlab.com/danabanana/api/pkg/application"
)

type AuthHandler struct {
	App         app.Application
	authService auth.UseCase
}

func NewAuthHandler(r *gin.RouterGroup, App app.Application, authService auth.UseCase) {
	authHandler := AuthHandler{
		App:         App,
		authService: authService,
	}

	r.POST("/auth", authHandler.Auth)
}

// Auth godoc
// @Tags Auth
// @Summary Auth by provided parameters
// @ID auth
// @Accept json
// @Param Auth body presenter.AuthRequest 1 "Auth data"
// @Produce json
// @Success 200 {object} presenter.TokenResponse
// @Failure 400 {object} presenter.BaseResponse
// @Failure 500 {string} message
// @Router /auth [post]
func (h *AuthHandler) Auth(ctx *gin.Context) {
	h.App.Logger.Trace("Auth", "start")

	var authRequest presenter.AuthRequest
	if err := ctx.ShouldBindJSON(&authRequest); err != nil {
		errResp := h.App.Logger.ErrorResponse(5000, err, h.App.Config.Logger.Locale)
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	response, errResp := h.authService.Auth(authRequest)
	if errResp != nil {
		ctx.JSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	h.App.Logger.Trace("Auth", "finish", response)
	ctx.JSON(response.HTTPStatus, response)
}
