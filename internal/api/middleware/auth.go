package middleware

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/usecase/auth"
	"gitlab.com/danabanana/api/internal/usecase/web/user"
	app "gitlab.com/danabanana/api/pkg/application"
	"net/http"
)

type AuthMiddleware struct {
	App         app.Application
	authService auth.UseCase
	userService user.UseCase
}

func NewAuthMiddleware(app app.Application, authService auth.UseCase, userService user.UseCase) *AuthMiddleware {

	return &AuthMiddleware{
		App:         app,
		authService: authService,
		userService: userService,
	}
}

func (m *AuthMiddleware) AuthorizeJWT(c *gin.Context) {
	const BEARER_SCHEMA = "Bearer"
	authHeader := c.GetHeader("Authorization")
	fmt.Println(authHeader)
	if len(authHeader) < len(BEARER_SCHEMA) {
		errResp := m.App.Logger.ErrorResponse(3005, errors.New("invalid authorization header"), m.App.Config.Logger.Locale)
		c.AbortWithStatusJSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	//tokenString := authHeader[len(BEARER_SCHEMA):]
	token, errResp := m.authService.ValidateToken(authHeader)
	if errResp != nil {
		c.AbortWithStatusJSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	if !token.Valid {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	claims := token.Claims.(jwt.MapClaims)

	c.Set("user-id", claims["userId"])
	c.Next()
}

func (m *AuthMiddleware) GetUserInfo(c *gin.Context) {
	userIDString, isOk := c.Get("user-id")
	if !isOk {
		errResp := m.App.Logger.ErrorResponse(3002, errors.New("incorrect userID received"), m.App.Config.Logger.Locale)
		c.AbortWithStatusJSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	userID := strfmt.UUID4(fmt.Sprintf("%v", userIDString))
	getUserResponse, errResp := m.userService.Get(userID)
	if errResp != nil {
		c.AbortWithStatusJSON(errResp.HTTPStatus, presenter.BaseResponse{
			Code:    errResp.Code,
			Message: errResp.ClientMessage,
		})
		return
	}

	c.Set("user", getUserResponse.Data)
	c.Next()
}
