package presenter

type AuthRequest struct {
	PhoneNumber string `json:"phoneNumber" binding:"required"`
}

type LoginRequest struct {
	PhoneNumber string `json:"phoneNumber" binding:"required"`
	Password    string `json:"password" binding:"required"`
}

type TokenResponse struct {
	BaseResponse
	Data string `json:"data"`
}
