package presenter

import (
	"gitlab.com/danabanana/api/internal/entity"
)

type CompanyCreateRequest struct {
	Name    string `json:"name"`
	Code    int    `json:"code"`
	Country string `json:"country"`
	Website string `json:"website"`
	Phone   string `json:"phone"`
}

type CompanyResponse struct {
	BaseResponse
	Data *entity.Company `json:"data"`
}

type CompanyListResponse struct {
	BaseResponse
	Data  []*entity.Company `json:"data"`
	Page  int               `json:"page"`
	Size  int               `json:"size"`
	Total int64             `json:"total"`
}

type CompanyUpdateRequest struct {
	Name    string `json:"name"`
	Code    int    `json:"code"`
	Country string `json:"country"`
	Website string `json:"website"`
	Phone   string `json:"phone"`
}
