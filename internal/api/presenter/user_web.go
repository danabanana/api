package presenter

import "gitlab.com/danabanana/api/internal/entity"

type UserCreateRequest struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type UserResponse struct {
	BaseResponse
	Data *entity.User `json:"data"`
}
