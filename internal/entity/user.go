package entity

type User struct {
	Base
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number" gorm:"unique"`
}
