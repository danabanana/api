package entity

import (
	"github.com/go-openapi/strfmt"
	"gorm.io/gorm"
	"time"
)

type Base struct {
	ID        strfmt.UUID4   `gorm:"primary_key;type:uuid;default:uuid_generate_v4()" json:"id" swaggertype:"string"`
	CreatedAt *time.Time     `json:"createdAt" gorm:"default:now()"`
	UpdatedAt *time.Time     `json:"updatedAt" gorm:"default:now()"`
	Deleted   gorm.DeletedAt `json:"deleted"`
}
