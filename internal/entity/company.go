package entity

type Company struct {
	Base
	Name    string `json:"name"`
	Code    int    `json:"code"`
	Country string `json:"country"`
	Website string `json:"website"`
	Phone   string `json:"phone"`
}

func (Company) TableName() string {
	return "company"
}
