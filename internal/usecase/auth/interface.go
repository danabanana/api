package auth

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
)

type UseCase interface {
	Auth(signupRequest presenter.AuthRequest) (response *presenter.TokenResponse, errResp *utilsModel.ErrorResponse)
	GenerateToken(user entity.User) (tokenStr string, errResp *utilsModel.ErrorResponse)
	ValidateToken(encodedToken string) (data *jwt.Token, errResp *utilsModel.ErrorResponse)
}
