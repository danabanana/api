package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	"gitlab.com/danabanana/api/internal/usecase/web/user"

	app "gitlab.com/danabanana/api/pkg/application"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
	"gorm.io/gorm"
	"time"
)

type Service struct {
	userRepo    user.Repository
	userService user.UseCase
	App         app.Application
}

func NewService(userRepo user.Repository, userService user.UseCase, app app.Application) *Service {
	return &Service{
		userRepo:    userRepo,
		userService: userService,
		App:         app,
	}
}

type AuthCustomClaims struct {
	UserID strfmt.UUID4 `json:"userId"`
	jwt.StandardClaims
}

func (s *Service) Auth(authRequest presenter.AuthRequest) (response *presenter.TokenResponse, errResp *utilsModel.ErrorResponse) {
	user, err := s.userRepo.GetByPhoneNumber(authRequest.PhoneNumber)

	// create user if doesn't exist
	if errors.Is(err, gorm.ErrRecordNotFound) {
		userCreateRequest := presenter.UserCreateRequest{
			PhoneNumber: authRequest.PhoneNumber,
		}

		userCreateResponse, errResp := s.userService.Create(userCreateRequest)
		if errResp != nil {
			return nil, errResp
		}

		user = userCreateResponse.Data

	}

	if user == nil || user.ID == "" {
		errResp = s.App.Logger.ErrorResponse(3003, errors.New("user is nil"), s.App.Config.Logger.Locale)
		return nil, errResp
	}

	// generate token
	accessToken, errResp := s.GenerateToken(*user)
	if errResp != nil {
		return nil, errResp
	}

	return &presenter.TokenResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: accessToken,
	}, nil
}

//GenerateToken generate token
func (s *Service) GenerateToken(user entity.User) (tokenStr string, errResp *utilsModel.ErrorResponse) {
	claims := &AuthCustomClaims{
		user.ID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(s.App.Config.Auth.TokenTTL)).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//encoded string
	t, err := token.SignedString([]byte(s.App.Config.Auth.SecretKey))
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(3000, err, s.App.Config.Logger.Locale)
		return
	}

	return t, nil
}

func (s *Service) ValidateToken(encodedToken string) (data *jwt.Token, errResp *utilsModel.ErrorResponse) {
	data, err := jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isvalid := token.Method.(*jwt.SigningMethodHMAC); !isvalid {
			return nil, errors.New("Invalid token")
		}
		return []byte(s.App.Config.Auth.SecretKey), nil
	})

	if err != nil {
		errResp = s.App.Logger.ErrorResponse(3001, err, s.App.Config.Logger.Locale)
		return nil, errResp
	}

	return data, nil
}
