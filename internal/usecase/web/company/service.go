package company

import (
	"encoding/json"
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	app "gitlab.com/danabanana/api/pkg/application"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
	"strings"
)

type Service struct {
	repo Repository
	App  app.Application
}

func NewService(r Repository, app app.Application) *Service {
	return &Service{
		repo: r,
		App:  app,
	}
}

//Create create company
func (s *Service) Create(request presenter.CompanyCreateRequest) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse) {
	if errResp != nil {
		return
	}

	companyToCreate := entity.Company{
		Name:    request.Name,
		Code:    request.Code,
		Country: request.Country,
		Website: request.Website,
		Phone:   request.Phone,
	}

	err := s.repo.Create(&companyToCreate)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(1000, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.CompanyResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: &companyToCreate,
	}, nil
}

//Get get company
func (s *Service) Get(id strfmt.UUID4) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse) {
	company, err := s.repo.Get(id)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(1001, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.CompanyResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: company,
	}, nil

}

//List list companies
func (s *Service) List(page int, size int, query string) (response *presenter.CompanyListResponse, errResp *utilsModel.ErrorResponse) {
	companies, total, err := s.repo.List(page, size, strings.ToLower(query))
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(1002, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.CompanyListResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data:  companies,
		Total: total,
		Page:  page,
		Size:  size,
	}, nil
}

//Update update company
func (s *Service) Update(request presenter.CompanyUpdateRequest) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse) {
	companyToUpdate := entity.Company{
		Name:    request.Name,
		Code:    request.Code,
		Country: request.Country,
		Website: request.Website,
		Phone:   request.Phone,
	}

	err := s.repo.Update(&companyToUpdate)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(1003, err, s.App.Config.Logger.Locale)
		return
	}

	jsonData, err := json.Marshal(companyToUpdate)
	if err != nil {
		return
	}

	err = s.App.RabbitMQ.Publisher("change", jsonData)
	if err != nil {
		return
	}

	return &presenter.CompanyResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: &companyToUpdate,
	}, nil
}

//Delete delete company
func (s *Service) Delete(id strfmt.UUID4) (response *presenter.BaseResponse, errResp *utilsModel.ErrorResponse) {
	err := s.repo.Delete(id)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(1004, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.BaseResponse{
		HTTPStatus: 200,
		Code:       0,
		Message:    "",
	}, nil
}
