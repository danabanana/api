package company

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/entity"
)

//MockRepo in memory repo
type MockRepo struct {
	m map[strfmt.UUID4]*entity.Company
}

//NewMockRepo create new repository
func NewMockRepo() *MockRepo {
	var m = map[strfmt.UUID4]*entity.Company{}
	return &MockRepo{
		m: m,
	}
}

//Create a company
func (r *MockRepo) Create(e *entity.Company) (*entity.Company, error) {
	e.ID = "f3e16904-e6b1-4f5f-8b74-75f17fb39455"
	r.m[e.ID] = e
	return e, nil
}

//Get a company
func (r *MockRepo) Get(id strfmt.UUID4) (*entity.Company, error) {
	if r.m[id] == nil {
		return nil, entity.ErrNotFound
	}
	return r.m[id], nil
}

//Update a company
func (r *MockRepo) Update(e *entity.Company) error {
	_, err := r.Get(e.ID)
	if err != nil {
		return err
	}
	r.m[e.ID] = e
	return nil
}

//List companies
func (r *MockRepo) List(page int, size int, query string) (companies []*entity.Company, total int64, err error) {

	var d []*entity.Company
	for _, j := range r.m {
		d = append(d, j)
	}
	return d, int64(len(d)), nil
}

//Delete a company
func (r *MockRepo) Delete(id strfmt.UUID4) error {
	if r.m[id] == nil {
		return entity.ErrNotFound
	}
	r.m[id] = nil
	return nil
}
