package company

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
)

type Reader interface {
	Get(id strfmt.UUID4) (company *entity.Company, err error)
	List(page int, size int, query string) (companies []*entity.Company, total int64, err error)
}

type Writer interface {
	Create(company *entity.Company) error
	Update(company *entity.Company) error
	Delete(id strfmt.UUID4) error
}

type Repository interface {
	Reader
	Writer
}

type UseCase interface {
	Create(request presenter.CompanyCreateRequest) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse)
	Get(id strfmt.UUID4) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse)
	List(page int, size int, query string) (response *presenter.CompanyListResponse, errResp *utilsModel.ErrorResponse)
	Update(request presenter.CompanyUpdateRequest) (response *presenter.CompanyResponse, errResp *utilsModel.ErrorResponse)
	Delete(id strfmt.UUID4) (response *presenter.BaseResponse, errResp *utilsModel.ErrorResponse)
}
