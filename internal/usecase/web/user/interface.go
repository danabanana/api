package user

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
)

type Reader interface {
	Get(id strfmt.UUID4) (user *entity.User, err error)
	GetByPhoneNumber(phoneNumber string) (user *entity.User, err error)
	IsPhoneNumberUnique(phoneNumber string) (isUnique bool, err error)
}

type Writer interface {
	Create(user *entity.User) error
}

type Repository interface {
	Reader
	Writer
}

type UseCase interface {
	Create(request presenter.UserCreateRequest) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse)
	Get(id strfmt.UUID4) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse)
	GetByPhoneNumber(phoneNumber string) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse)
}
