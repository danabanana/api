package user

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/entity"
)

//MockRepo in memory repo
type MockRepo struct {
	m map[strfmt.UUID4]*entity.User
}

//NewMockRepo create new repository
func NewMockRepo() *MockRepo {
	var m = map[strfmt.UUID4]*entity.User{}
	return &MockRepo{
		m: m,
	}
}

//Create a user
func (r *MockRepo) Create(e *entity.User) (*entity.User, error) {
	e.ID = "f3e16904-e6b1-4f5f-8b74-75f17fb39455"
	r.m[e.ID] = e
	return e, nil
}

//Get a user
func (r *MockRepo) Get(id strfmt.UUID4) (*entity.User, error) {
	if r.m[id] == nil {
		return nil, entity.ErrNotFound
	}
	return r.m[id], nil
}

//Update a user
func (r *MockRepo) Update(e *entity.User) error {
	_, err := r.Get(e.ID)
	if err != nil {
		return err
	}
	r.m[e.ID] = e
	return nil
}

//List users
func (r *MockRepo) List(query string) ([]*entity.User, error) {
	var d []*entity.User
	for _, j := range r.m {
		d = append(d, j)
	}
	return d, nil
}

//Delete a user
func (r *MockRepo) Delete(id strfmt.UUID4) error {
	if r.m[id] == nil {
		return entity.ErrNotFound
	}
	r.m[id] = nil
	return nil
}
