package user

import (
	"errors"
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/api/presenter"
	"gitlab.com/danabanana/api/internal/entity"
	app "gitlab.com/danabanana/api/pkg/application"
	utilsModel "gitlab.com/danabanana/utils/pkg/model"
	"gorm.io/gorm"
)

type Service struct {
	userRepo Repository
	App      app.Application
}

func NewService(userRepo Repository, app app.Application) *Service {
	return &Service{
		userRepo: userRepo,
		App:      app,
	}
}

//Create create user
func (s *Service) Create(request presenter.UserCreateRequest) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse) {
	isPhoneNumberUnique, err := s.userRepo.IsPhoneNumberUnique(request.PhoneNumber)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(4004, err, s.App.Config.Logger.Locale)
		return
	}

	// phone number is not unique
	if isPhoneNumberUnique == false {
		errResp = s.App.Logger.ErrorResponse(4003, err, s.App.Config.Logger.Locale)
	}

	userToCreate := entity.User{
		Name:        request.Name,
		PhoneNumber: request.PhoneNumber,
	}

	err = s.userRepo.Create(&userToCreate)
	if err != nil {
		errResp = s.App.Logger.ErrorResponse(4002, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.UserResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: &userToCreate,
	}, nil
}

//Get get user
func (s *Service) Get(id strfmt.UUID4) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse) {
	user, err := s.userRepo.Get(id)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		errResp = s.App.Logger.ErrorResponse(4000, err, s.App.Config.Logger.Locale)
		return
	}

	if err != nil {
		errResp = s.App.Logger.ErrorResponse(4001, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.UserResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: user,
	}, nil

}

//Get get user
func (s *Service) GetByPhoneNumber(phoneNumber string) (response *presenter.UserResponse, errResp *utilsModel.ErrorResponse) {
	user, err := s.userRepo.GetByPhoneNumber(phoneNumber)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		errResp = s.App.Logger.ErrorResponse(4005, err, s.App.Config.Logger.Locale)
		return
	}

	if err != nil {
		errResp = s.App.Logger.ErrorResponse(4006, err, s.App.Config.Logger.Locale)
		return
	}

	return &presenter.UserResponse{
		BaseResponse: presenter.BaseResponse{
			HTTPStatus: 200,
			Code:       0,
			Message:    "",
		},
		Data: user,
	}, nil

}
