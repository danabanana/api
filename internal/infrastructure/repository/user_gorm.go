package repository

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/entity"
	"gorm.io/gorm"
)

type UserGormRepository struct {
	DB *gorm.DB
}

func NewUserGormRepository(db *gorm.DB) *UserGormRepository {
	return &UserGormRepository{
		DB: db,
	}
}

func (r *UserGormRepository) Create(user *entity.User) (err error) {
	return r.DB.Create(&user).Error
}

func (r *UserGormRepository) Get(id strfmt.UUID4) (user *entity.User, err error) {
	err = r.DB.First(&user, "id = ?", id).Error
	return
}

func (r *UserGormRepository) GetByPhoneNumber(phoneNumber string) (user *entity.User, err error) {
	err = r.DB.First(&user, "phone_number = ?", phoneNumber).Error
	return
}

func (r *UserGormRepository) Update(user *entity.User) error {
	return r.DB.Model(&user).Updates(&user).Error
}

func (r *UserGormRepository) IsPhoneNumberUnique(phoneNumber string) (isUnique bool, err error) {
	var usersFound []entity.User
	err = r.DB.Where("phone_number = ?", phoneNumber).Find(&usersFound).Error
	if usersFound != nil || len(usersFound) == 0 {
		isUnique = true
	}
	return
}
