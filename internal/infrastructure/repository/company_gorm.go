package repository

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/entity"
	"gorm.io/gorm"
)

type CompanyGormRepository struct {
	DB *gorm.DB
}

func NewCompanyGormRepository(db *gorm.DB) *CompanyGormRepository {
	return &CompanyGormRepository{
		DB: db,
	}
}

//Create a company
func (r *CompanyGormRepository) Create(company *entity.Company) (err error) {
	return r.DB.Create(&company).Error
}

//Get a company
func (r *CompanyGormRepository) Get(id strfmt.UUID4) (company *entity.Company, err error) {
	err = r.DB.First(&company, "id = ?", id).Error
	return
}

// List companies
func (r *CompanyGormRepository) List(page int, size int, query string) (companies []*entity.Company, total int64, err error) {
	offset := (page - 1) * size
	err = r.DB.Limit(size).Offset(offset).Order("created_at desc").Find(&companies).Error
	r.DB.Model(&entity.Company{}).Count(&total)
	return
}

//Update a company
func (r *CompanyGormRepository) Update(company *entity.Company) error {
	return r.DB.Model(&company).Updates(&company).Error
}

//Delete a company
func (r *CompanyGormRepository) Delete(id strfmt.UUID4) error {
	return r.DB.Delete(&entity.Company{}, id).Error
}
