package repository

import (
	"fmt"
	"gorm.io/gorm/logger"

	"gitlab.com/danabanana/api/internal/entity"
	"gitlab.com/danabanana/api/pkg/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Gorm struct {
	DB *gorm.DB
}

func NewGorm(config *config.Config) (g Gorm, err error) {
	connectionString := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=%v TimeZone=Asia/Almaty", config.DB.Host, config.DB.Port, config.DB.User, config.DB.Name, config.DB.Pass, config.DB.Mode)

	db, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{})
	if err != nil {
		return
	}

	if config.DB.LogMode == true {
		db.Logger = logger.Default.LogMode(logger.Info)
	}

	return Gorm{
		DB: db,
	}, err
}

func (g *Gorm) AutoMigrate() (err error) {
	return g.DB.AutoMigrate(
		entity.Company{},
	)
}

func (g *Gorm) AutoMigrateData() error {
	//// upsert company
	//if err := g.DB.Clauses(clause.OnConflict{}).Create(&entity.Company{}).Error; err != nil {
	//	return errx
	//}
	//
	return nil
}
