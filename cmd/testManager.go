package cmd

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var testCmd = &cobra.Command{
	Use:   "test-manager",
	Short: "Start manager test",
	Long:  "Starts test for managers",
	Run: func(cmd *cobra.Command, args []string) {
		gormTestPath := os.Getenv("GOPATH") + "/src/gitlab.com/danabanana/api/test/usecase"

		outCmd := exec.Command("ginkgo")
		outCmd.Dir = gormTestPath
		out, err := outCmd.Output()
		if err != nil {
			fmt.Printf("%s", err)
		}

		fmt.Println("Gorm test init")
		output := string(out[:])
		fmt.Println(output)
	},
}

func init() {
	RootCmd.AddCommand(testCmd)
}
