package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/danabanana/api/internal/api"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start HTTP server",
	Long:  "Starts a http server and serves the configured business-api",
	Run: func(cmd *cobra.Command, args []string) {
		server, err := api.NewServer()
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
		server.Start()
	},
}

func init() {
	RootCmd.AddCommand(serveCmd)
}
