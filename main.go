package main

import (
	"gitlab.com/danabanana/api/cmd"
	_ "gitlab.com/danabanana/api/docs"
)

// @title Swagger sample API
// @version v0.0.1-dev
// @description sample API Documentation.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email dana@email.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /v1
// @query.collection.companyat multi

func main() {
	cmd.Execute()
}
