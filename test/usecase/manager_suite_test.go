package usecase_test

import (
	"gitlab.com/danabanana/api/pkg/application"
	"testing"
)

func TestManager(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Manager Suite")
}

var _ = BeforeSuite(func() {
	// init App
	_, err := application.NewTestApplication()
	Expect(err).NotTo(HaveOccurred())
	//manager, err := internalManager.Get(App)
	//Expect(err).NotTo(HaveOccurred())

})
