package usecase_test

import (
	"github.com/go-openapi/strfmt"
	"gitlab.com/danabanana/api/internal/entity"
	"gitlab.com/danabanana/api/internal/usecase/web/company"
	"net/http"
	"time"
)

func NewFixtureCompany() entity.Company {
	currentTime := time.Now()
	return entity.Company{
		Base: entity.Base{
			CreatedAt: &currentTime,
			UpdatedAt: &currentTime,
		},
	}
}

var _ = Describe("Company", func() {
	Context("Create", func() {
		When("sent correct request body", func() {
			It("should return created company", func() {
				repo := company.NewMockRepo()

				service := company.NewService(repo)
				company := NewFixtureCompany()

				response, errResp := service.CreateCompany(company)
				Expect(errResp).To(BeNil())
				Expect(response).NotTo(BeNil())
				Expect(response.HTTPStatus).To(Equal(http.StatusOK))
				Expect(response.Data.ID).NotTo(Equal(strfmt.UUID4(0)))
			})
		})
	})
})
